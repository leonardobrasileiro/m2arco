<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'crypt' => [
        'key' => 'd8fe58b5b989403a4f3226ba08039cb2'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'database',
                'dbname' => 'magento',
                'username' => 'root',
                'password' => 'mysql',
                'active' => '1'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'developer',
    'session' => [
        'save' => 'redis',
        'redis' => [
            'host' => 'redis',
            'port' => '6379',
            'password' => '',
            'timeout' => '2.5',
            'persistent_identifier' => '',
            'database' => '0',
            'compression_threshold' => '2048',
            'compression_library' => 'gzip',
            'log_level' => '1',
            'max_concurrency' => '6',
            'break_after_frontend' => '5',
            'break_after_adminhtml' => '30',
            'first_lifetime' => '600',
            'bot_first_lifetime' => '60',
            'bot_lifetime' => '7200',
            'disable_locking' => '0',
            'min_lifetime' => '60',
            'max_lifetime' => '2592000'
        ]
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => 'redis',
                    'database' => '1',
                    'port' => '6379'
                ]
            ]
        ]
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => null
        ]
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1
    ],
    'http_cache_hosts' => [
        [
            'host' => 'localhost',
            'port' => '6081'
        ]
    ],
    'cron_consumers_runner' => [
        'cron_run' => false
    ],
    'install' => [
        'date' => 'Fri, 12 Jul 2019 14:18:00 +0000'
    ],
    'system' => [
        'default' => [
            'system' => [
                'smtp' => [
                    'host' => 'localhost',
                    'port' => '25'
                ]
            ],
            'country' => [
                'default' => 'BR'
            ],
            'web' => [
                'unsecure' => [
                    'base_url' => 'http://arco-backend.magento2.localhost/',
                    'base_link_url' => '{{unsecure_base_url}}',
                    'base_static_url' => '',
                    'base_media_url' => ''
                ],
                'secure' => [
                    'base_url' => 'http://arco-backend.magento2.localhost/',
                    'base_link_url' => '{{secure_base_url}}',
                    'base_static_url' => '',
                    'base_media_url' => '',
                    'use_in_frontend' => 0,
                    'use_in_adminhtml' => 0,
                    'offloader_header' => 'X-Forwarded-Proto'
                ],
                'default' => [
                    'front' => 'cms'
                ],
                'cookie' => [
                    'cookie_lifetime' => 3600,
                    'cookie_path' => '',
                    'cookie_domain' => 'arco-backend.magento2.localhost',
                    'cookie_httponly' => 1
                ]
            ],
            'locale' => [
                'timezone' => 'America/Fortaleza',
                'code' => 'pt_BR',
                'weight_unit' => 'kgs'
            ]
        ]
    ]
];
