# E-Commerce Magento 2 Arco

## Banco de dados

Ponha um dump do banco de dados em `docker/mysql/initdb`. São aceitos arquivos .sql e .sql.gz

## Projeto Magento

Este projeto cria apenas o ambiente de containers. O projeto Magento 2 deve ficar na pasta `magento2`.
No Linux, sugiro criar um link simbólico da seguinte forma:

```
$ ln -s <PATH_DO_PROJETO_MAGENTO> magento2 
``` 

## Arquivo env.php

Copie o arquivo docker/mysql/env.php para magento2/app/etc/

## Arquivo de hosts

```
127.0.0.1  arco-backend.magento2.localhost
```

## Iniciar containers

Para instalar este projeto, é necessário ter o docker e docker-compose instalados.

Inicie os containers. Vai demorar um pouco até baixar as imagens e importar o banco de dados.

```
docker-compose up -d
```

Existem alguns atalhos na pasta `docker/` para utilitários que rodam dentro dos containers. O comando abaixo vai baixar 
as dependências. A instalação vai pedir a autenticação no repositório do Magento que pode ser obtida [aqui](https://magento.com/).

```
docker/composer install
```

Os comandos abaixo vão atualizar o banco de dados com as últimas definições e gerar os arquivos de injeção de 
dependência.

``` 
docker/magento setup:upgrade
docker/magento setup:di:compile
```

Alguns arquivos e pastas ficarão com permissões muito restritas para desenvolvimento. Rode o script abaixo para liberar. 
Mas atenção, este script só é válido para o ambiente do desenvolvedor.

```
sudo docker/set-file-permissions
```

## Exibição de mídia

system/media_storage_configuration/media_storage
com valor 2

## PHP Debug com Intellij/PHPStorm

A máquina virtual tem o [Xdebug](https://xdebug.org/) habilitado para a execução passo a passo
do PHP. Abaixo vai uma orientação da configuração do ambiente para as IDEs da JetBrains. O
Intellij Idea Ultimate possui um plugin para PHP que não é disponível para a versão Community
então este guia só se aplica à primeira ou ao PHPStorm.

1. Vá em File->Settings->PHP->Debug. Certifique-se que a seção Xdebug está habilitado e escutando
na porta 9001.

2. Vá em File->Settings->PHP->Servers. Crie um novo server para cada dominio porta 80. Habilite
a opção 'Use Path Mappings'. Na pasta raiz html do projeto (m2arco/magento2), digite o mapeamento `/var/www/html`.
Aplique as alterações.

3. Na janela principal, próximo aos botões de start e debug, clique no botão 'Start Listening
for PHP Debug Connections'. Agora sua IDE está pronta para realizar o debug.

4. Para iniciar o debug, o browser precisa enviar alguns parâmetros para o servidor.
O plugin Xdebug Helper pode ser usado no Chrome. Após instalá-lo, um 'bug' aparecerá no canto
superior direito da janela. Clique nele e selecione a opção Debug.

## Envio de emails

Os emails do Magento são direcionados para o container do [Mailhog](https://github.com/mailhog/MailHog). Ele pode ser acessado em
http://localhost:8025.

Os dados do SMTP Mailhog são:

- Hostname: mailhog
- Port: 1025
- Authentication: none

## Braspag Sandbox

```
update arco_payment_configuration 
set braspag_merchant_id = '3EDE3A52-CB01-41D4-8CD3-F5D22B23F2FE', 
    braspag_merchant_key = 'useivtc6bhhqvv0cBJ6cQzDgszeklfywBHAYg607', 
    braspag_provider_debitcard = 'Simulado', 
    braspag_provider_creditcard = 'Simulado',
    braspag_provider_creditcard_hiper = 'Simulado', 
    braspag_provider_creditcard_banese = 'Simulado',
    braspag_provider_boleto = 'Simulado'
where website_id = 1;

update arco_payment_configuration 
set braspag_merchant_id = '764d767e-7111-4cab-ab34-c726038d9dd1', 
    braspag_merchant_key = 'CHi8MJ4wYSKj6VDin9j7gIVAVCKG3ZoMVIqWPo3R', 
    braspag_provider_debitcard = 'Simulado', 
    braspag_provider_creditcard = 'Simulado',
    braspag_provider_creditcard_hiper = 'Simulado', 
    braspag_provider_creditcard_banese = 'Simulado',
    braspag_provider_boleto = 'Simulado'
where website_id = 2;
```
