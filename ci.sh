#!/usr/bin/env sh
# @author leo@lesteti.com.br

set -e # Exit immediately if a command exits with a non-zero status.

MAGE_DIR='magento2'
SHARED_DIR='/var/www/html/shared'

echo "# Run composer install"

composer --quiet --working-dir=$MAGE_DIR install

echo "# Run setup:di:compile"

rm $MAGE_DIR/app/etc/env.php
php $MAGE_DIR/bin/magento setup:di:compile --quiet

echo "# Remove shared dirs"

rm $MAGE_DIR/app/etc/env.php
rm -rf $MAGE_DIR/var/log
rm -rf $MAGE_DIR/var/page_cache
rm -rf $MAGE_DIR/var/cache
rm -rf $MAGE_DIR/var/export
rm -rf $MAGE_DIR/var/report
rm -rf $MAGE_DIR/var/import_history
rm -rf $MAGE_DIR/var/session
rm -rf $MAGE_DIR/var/importexport
rm -rf $MAGE_DIR/var/backups
rm -rf $MAGE_DIR/var/tmp
rm -rf $MAGE_DIR/pub/sitemaps
rm -rf $MAGE_DIR/pub/media
